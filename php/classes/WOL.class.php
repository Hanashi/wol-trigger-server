<?php
class WOL
{
	private $_DB;
	
	public function __construct($DB)
	{
		$this->_DB = $DB;
	}
	
	public function StartPC($Mac)
	{
		$count = $this->_DB->QuerySelect("SELECT COUNT(wol_id) AS count FROM systems WHERE mac_adress = :Mac", array(":Mac" => $Mac));
		if ($count[0]["count"] == 0)
		{
			return $this->_DB->Query("INSERT INTO systems (mac_adress, status) VALUES (:Mac, '1')", array(":Mac" => $Mac));
		}
		else
		{
			return $this->_DB->Query("UPDATE systems SET status = '1' WHERE mac_adress = :Mac", array(":Mac" => $Mac));
		}
	}
	
	public function GetPCs()
	{
		$row = $this->_DB->QuerySelect("SELECT mac_adress FROM systems WHERE status = '1'");
		return json_encode($row);
	}
	
	public function ResetStatus($Mac)
	{
		return $this->_DB->Query("UPDATE systems SET status = '0' WHERE mac_adress = :Mac", array(":Mac" => $Mac));
	}
}
?>