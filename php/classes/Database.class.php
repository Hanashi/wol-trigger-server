<?php
class Database extends PDO
{
	public function __construct($Hostname, $Database, $Username, $Password)
	{
		try
		{
			parent::__construct(sprintf ("pgsql:dbname=%s host=%s", $Database, $Hostname), $Username, $Password);
			$this->Query("SET application_name TO 'Titan App Server';");
		}
		catch (PDOException $e)
		{
			die($e->getMessage());
		}
	}
	
	public function QuerySelect($Query, $Data = array())
	{
		$Prepared = $this->prepare($Query);
		$Prepared->execute($Data);
		$ErrorInfo = $this->errorInfo();
		if (strlen($ErrorInfo[2]) > 0)
			die($ErrorInfo[2]);
		return $Prepared->fetchAll(PDO::FETCH_NAMED);
	}
	
	public function Query($Query, $Data = array())
	{
		$IsExecuted = false;
		try
		{
			$Prepared = $this->prepare($Query);
			$this->beginTransaction();
			$IsExecuted = $Prepared->execute($Data);
			$this->commit();
			$ErrorInfo = $this->errorInfo();
			if (strlen($ErrorInfo[2]) > 0)
				die($ErrorInfo[2]);
		}
		catch (Exception $e)
		{
			$this->rollBack();
			die($e->getMessage());
		}
		return $IsExecuted;
	}
}
?>
