<?php
class Server
{
	private $_DB;
	
	public function __construct($DB)
	{
		$this->_DB = $DB;
	}
	
	public function HandleRequest()
	{
		if (isset($_POST["type"]) && $_POST["type"] == "WOL")
		{
			return $this->HandleWolRequests();
		}
		else
		{
			return "error_undefined_type";
		}
	}
	
	private function HandleWolRequests()
	{
		if (isset($_POST["command"]) && $_POST["command"] == "StartPC")
		{
			$wol = new WOL($this->_DB);
			if ($wol->StartPC($_POST["Mac"]))
			{
				return "ok_db_updated";
			}
			else
			{
				return "error_fatal_db";
			}
			$wol = null;
		}
		else if (isset($_POST["command"]) && $_POST["command"] == "GetPCs")
		{
			$wol = new WOL($this->_DB);
			return $wol->GetPCs();
			$wol = null;
		}
		else if (isset($_POST["command"]) && $_POST["command"] == "ResetStatus")
		{
			$wol = new WOL($this->_DB);
			if ($wol->ResetStatus($_POST["Mac"]))
			{
				return "ok_db_updated";
			}
			else
			{
				return "error_fatal_db";
			}
			$wol = null;
		}
		else
		{
			return "error_unknown_command";
		}
	}
	
	public function __destruct()
	{
		$this->_DB = null;
	}
}
?>