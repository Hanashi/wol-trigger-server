<?php
class Main
{
	private $_DB;
	private $_DatabaseHostname;
	private $_DatabaseName;
	private $_DatabaseUser;
	private $_DatabasePassword;
	
	public function __construct()
	{
		$this->ReadConfig();
		$this->InitDatabase();
		$this->StartServer();
	}
	
	private function ReadConfig()
	{
		$config = new Config("configs/Database.Config.xml");
		$configs = $config->Read();
		
		$this->_DatabaseHostname 	= $configs["DatabaseHostname"];
		$this->_DatabaseName 		= $configs["DatabaseName"];
		$this->_DatabaseUser 		= $configs["DatabaseUser"];
		$this->_DatabasePassword 	= $configs["DatabasePassword"];
		$config = null;
	}
	
	private function InitDatabase()
	{
		$this->_DB = new Database($this->_DatabaseHostname, $this->_DatabaseName, $this->_DatabaseUser, $this->_DatabasePassword);
	}
	
	private function StartServer()
	{
		$server = new Server($this->_DB);
		echo $server->HandleRequest();
		$server = null;
	}
	
	public function __destruct()
	{
		$this->_DB = null;		
	}
}
?>