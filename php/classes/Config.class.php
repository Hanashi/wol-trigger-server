<?php
class Config
{
	private $_ConfigFile;
	
	public function __construct($ConfigFile)
	{
		$this->_ConfigFile = $ConfigFile;
	}
	
	public function Read()
	{
		if (file_exists($this->_ConfigFile))
		{
			$xml = simplexml_load_file($this->_ConfigFile);
			
			return array("DatabaseHostname"	=> (string)$xml->Hostname,
						 "DatabaseName"		=> (string)$xml->Name,
						 "DatabaseUser"		=> (string)$xml->Username,
						 "DatabasePassword"	=> (string)$xml->Password);
		}
		else
		{
			exit("Die Konfigurationsdatei ist keien gültige Datei.");
		}
	}
}
?>