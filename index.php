<?php
ini_set("display_errors", 1);

function __autoload($class)
{
	require_once(sprintf("php/classes/%s.class.php", $class));
}

$main = new Main();
?>