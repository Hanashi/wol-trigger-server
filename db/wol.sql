/*
Navicat PGSQL Data Transfer

Source Server         : Shepard
Source Server Version : 90109
Source Host           : localhost:5432
Source Database       : wol
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90109
File Encoding         : 65001

Date: 2013-06-25 10:19:55
*/


-- ----------------------------
-- Sequence structure for systems_wol_id_seq
-- ----------------------------
DROP SEQUENCE "public"."systems_wol_id_seq";
CREATE SEQUENCE "public"."systems_wol_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."systems_wol_id_seq"', 1, true);

-- ----------------------------
-- Table structure for systems
-- ----------------------------
DROP TABLE IF EXISTS "public"."systems";
CREATE TABLE "public"."systems" (
"wol_id" int4 DEFAULT nextval('systems_wol_id_seq'::regclass) NOT NULL,
"mac_adress" varchar(50) COLLATE "default",
"status" int2 DEFAULT 0
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."systems_wol_id_seq" OWNED BY "systems"."wol_id";

-- ----------------------------
-- Primary Key structure for table systems
-- ----------------------------
ALTER TABLE "public"."systems" ADD PRIMARY KEY ("wol_id");
