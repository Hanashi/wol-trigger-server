<?php
function SendPost($url, $request)
{
	$fields_string = null;
	foreach ($request as $key => $value)
	{
		$fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, count($request));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);
	
	return $result;
}

$url = "https://****.**/wol/";

$GetPCsArray = array("type" 	=> "WOL",
					 "command"	=> "GetPCs");
					 
$PCs = json_decode(SendPost($url, $GetPCsArray));

foreach ($PCs as &$PC)
{
	exec(sprintf("wakeonlan %s", $PC->mac_adress), $output);
	
	$ResetPcs = array("type" 	=> "WOL",
					  "command"	=> "ResetStatus",
					  "Mac"		=> $PC->mac_adress);
	SendPost($url, $ResetPcs);
}
?>